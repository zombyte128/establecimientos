<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categorias extends Model
{
    public function establecimientos()
    {
        return $this->hasMany('App\Establecimiento');
    }

    //leer rutas por el slug

    public function getRouteKeyName()
    {
        return 'slug';
    }
}
