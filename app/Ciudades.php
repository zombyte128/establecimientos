<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ciudades extends Model
{
    public function departamentos()
    {
    	return $this->belongsTo('App\Departamentos','departamento_id');
    }
}
