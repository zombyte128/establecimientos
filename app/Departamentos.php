<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Departamentos extends Model
{
    public function paises()
    {
    	return $this->belongsTo('App\Paises','pais_id');
    }

    public function ciudades()
    {
        return $this->hasMany('App\Ciudades');
    }
}
