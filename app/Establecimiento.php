<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Establecimiento extends Model
{
    public $table='establecimientos';

    protected $fillable = [
        'nombre_establecimiento','categoria_id','imagen_principal','direccion','colonia','latitud','longitud','numero_telefono','descripcion','hora_apertura','hora_cierre','uuid','user_id'
    ];

    public function users()
    {
       return $this->belongsTo('App\User','user_id');
    }

    public function categorias()
    {
        return $this->belongsTo('App\Categorias','categoria_id');
    }
}
