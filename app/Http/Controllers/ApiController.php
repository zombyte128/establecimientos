<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categorias;
use App\Establecimiento;
use App\Imagen;

class ApiController extends Controller
{
    public function index()
    {
        $establecimientos = Establecimiento::with('categorias')->get();
        return response()->json($establecimientos);

    }

    public function categorias()
    {
        $categorias = Categorias::all();
        return response()->json($categorias);

    }

    public function categoria( Categorias $categoria)
    {
        $establecimientos = Establecimiento::where('categoria_id',$categoria->id)->with('categorias')->take(3)->get();
        return response()->json($establecimientos);
    }

    public function establecimientocategoria(Categorias $categoria)
    {
        $establecimientos = Establecimiento::where('categoria_id',$categoria->id)->with('categorias')->get();
        return response()->json($establecimientos);

    }

    //mostrar un establecimiento en especifico
    public function show( Establecimiento $establecimiento)
    {
        $imagenes = Imagen::where('id_establecimiento',$establecimiento->uuid)->get();
        $establecimiento->imagenes = $imagenes;
        return response()->json($establecimiento);

    }
}
