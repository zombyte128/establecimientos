<?php

namespace App\Http\Controllers;

use App\Establecimiento;
use Illuminate\Http\Request;
use App\Categorias;
use App\Imagen;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;

class EstablecimientoController extends Controller
{
    public function index()
    {
        //
    }

    
    public function create()
    {
        $categorias = Categorias::all();
        return view('establecimientos.create',compact('categorias'));
    }

    
    public function store(Request $request)
    {
        $data = $request->validate([
            'nombre_establecimiento' => 'required',
            'categoria_id' => 'required|exists:App\Categorias,id',
            'imagen_principal' => 'required|image|max:1000',
            'direccion' => 'required',
            'latitud' => 'required',
            'longitud' => 'required',
            'numero_telefono' => 'required|numeric',
            'descripcion' => 'required',
            'hora_apertura' => 'required|date_format:H:i',
            'hora_cierre' => 'required|date_format:H:i|after:hora_apertura',
            'uuid' => 'required|uuid'
        ]);
 
        //Guardar la imagen
        $ruta_imagen = $request['imagen_clinica']->store('principales','public');

        //Rezise de la imagen
        $img = Image::make(public_path("storage/{$ruta_imagen}"))->fit(800,600);
        $img->save();

        //guardar datos en la base de datos 
        $establecimientos = new Establecimiento();
        $establecimientos->user_id = Auth::user()->id;
        $establecimientos->nombre_establecimiento = $request->nombre_establecimiento;
        $establecimientos->categoria_id = $request->categoria_id;
        $establecimientos->imagen_principal = $ruta_imagen;
        $establecimientos->direccion = $request->direccion;
        $establecimientos->latitud = $request->latitud;
        $establecimientos->longitud = $request->longitud;
        $establecimientos->numero_telefono = $request->numero_telefono;
        $establecimientos->descripcion = $request->descripcion;
        $establecimientos->hora_apertura = $request->hora_apertura;
        $establecimientos->hora_cierre = $request->hora_cierre;
        $establecimientos->uuid = $request->uuid;
        $establecimientos->save();

        return back()->with('estado','Tu información se almaceno correctamente');
    }

    public function show(Establecimiento $establecimiento)
    {
        //
    }

    public function edit(Establecimiento $establecimiento)
    {
        //consulta las categorias
        $categorias = Categorias::all();

        //obtener establecimiento
        $establecimiento = Auth::user()->establecimientos;
        $establecimiento->hora_apertura = date('H:i',strtotime($establecimiento->hora_apertura));
        $establecimiento->hora_cierre = date('H:i',strtotime($establecimiento->hora_cierre));

        //obtener imagenes del establecimiento
        $imagenes = Imagen::where('id_establecimiento',$establecimiento->uuid)->get();

        return view('establecimientos.edit',compact('categorias','establecimiento','imagenes'));
    }

    public function update(Request $request, Establecimiento $establecimiento)
    {
        //ejecutar el policy
        $this->authorize('update',$establecimiento);
        
        $data = $request->validate([
            'nombre_establecimiento' => 'required',
            'categoria_id' => 'required|exists:App\Categorias,id',
            'imagen_principal' => 'image|max:1000',
            'direccion' => 'required',
            'latitud' => 'required',
            'longitud' => 'required',
            'numero_telefono' => 'required|numeric',
            'descripcion' => 'required',
            'hora_apertura' => 'required|date_format:H:i',
            'hora_cierre' => 'required|date_format:H:i|after:hora_apertura',
            'uuid' => 'required|uuid'
        ]);

        $establecimiento->nombre_establecimiento = $request->nombre_establecimiento;
        $establecimiento->categoria_id = $request->categoria_id;
        $establecimiento->direccion = $request->direccion;
        $establecimiento->latitud = $request->latitud;
        $establecimiento->longitud = $request->longitud;
        $establecimiento->numero_telefono = $request->numero_telefono;
        $establecimiento->descripcion = $request->descripcion;
        $establecimiento->hora_apertura = $request->hora_apertura;
        $establecimiento->hora_cierre = $request->hora_cierre;
        $establecimiento->uuid = $request->uuid;

        if($request->imagen_principal){
            //Guardar la imagen
            $ruta_imagen = $request['imagen_principal']->store('principales','public');

            //Rezise de la imagen
            $img = Image::make(public_path("storage/{$ruta_imagen}"))->fit(800,600);
            $img->save();
            $establecimiento->imagen_principal = $ruta_imagen;
        }

        $establecimiento->save();

        //mensaje al usuario
        return back()->with('estado','su informacion se edito correctamente');
    }

    public function destroy(Establecimiento $establecimiento)
    {
        //
    }
}
