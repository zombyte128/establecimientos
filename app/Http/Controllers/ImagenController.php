<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use App\Imagen;
use Illuminate\Support\Facades\File;
use App\Establecimiento;

class ImagenController extends Controller
{
    public function store(Request $request)
    {
        //leer la imagen
        $ruta_imagen = $request->file('file')->store('establecimientos','public');

        //Resize a la imagen
        $imagen = Image::make(public_path("storage/{$ruta_imagen}"))->fit(800,450);
        $imagen->save();

        //almacenar con modelo
        $imagenDB = new Imagen;
        $imagenDB->id_establecimiento = $request['uuid'];
        $imagenDB->ruta_imagen = $ruta_imagen;
        $imagenDB->save();

        //retornar respuesta
        $respuesta = [
            'archivo' => $ruta_imagen
        ];

        //resize a la imagen
        return response()->json($respuesta);
    }

    public function destroy(Request $request)
    {
        $imagen = $request->get('imagen');
        //validacion
        $uuid = $request->get('uuid');
        $establecimiento = Establecimiento::where('uuid',$uuid)->first();
        $this->authorize('delete',$establecimiento);

        if(File::exists('storage/'.$imagen))
        {
            //elimina imagen del servidor
            File::delete('storage/'.$imagen);

            //elimina image de la base de datos
            Imagen::where('ruta_imagen',$imagen)->delete();

            $respuesta = [
                'mensaje' => 'Imagen eliminada',
                'imagen' => $imagen
            ];
        }

        return response()->json($respuesta);
    }
}
