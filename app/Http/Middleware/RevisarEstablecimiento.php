<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Closure;

class RevisarEstablecimiento
{
    public function handle($request, Closure $next)
    {
        if(Auth::user()->establecimientos)
        {
            return redirect('/establecimientos/edit');
        }
        return $next($request);
    }
}
