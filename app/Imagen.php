<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Imagen extends Model
{
    public $table = 'imagens';

    public $fillable = [
        'id_establecimiento','ruta_imagen','created_at','update_at'
    ];
}
