<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paises extends Model
{
    public function departamentos()
    {
        return $this->hasMany('App\Departamentos');
    }
}
