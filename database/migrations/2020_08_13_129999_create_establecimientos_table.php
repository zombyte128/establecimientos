<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstablecimientosTable extends Migration
{
    
    public function up()
    {
        Schema::create('establecimientos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedBigInteger('categoria_id');
            $table->foreign('categoria_id')->references('id')->on('categorias')->onDelete('cascade');
            $table->string('nombre_establecimiento');
            $table->string('imagen_principal');
            $table->string('direccion');
            $table->string('latitud');
            $table->string('longitud');
            $table->string('numero_telefono');
            $table->text('descripcion');
            $table->time('hora_apertura');
            $table->time('hora_cierre');
            $table->uuid('uuid');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('establecimientos');
    }
}
