<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class UserTableSeeder extends Seeder
{
   
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'David Chavarria',
            'email' => 'david.chavarria2@gmail.com',
            'email_verified_at' => Carbon::now(),
            'password' => bcrypt('123456'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('users')->insert([
            'name' => 'Oswaldo Calzada',
            'email' => 'david.chavarria2@hotmail.com',
            'email_verified_at' => Carbon::now(),
            'password' => bcrypt('123456'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
    }
}
