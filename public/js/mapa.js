document.addEventListener('DOMContentLoaded',() => {

    const provider = new GeoSearch.OpenStreetMapProvider();

    if(document.querySelector('#mapa'))
    {
        
        //estas variables son para dejar marcadas las coordenadas en el mapa
        const lat = document.querySelector('#latitud').value === '' ? 13.7003861 : document.querySelector('#latitud').value;
        const lng = document.querySelector('#longitud').value === '' ? -89.2245154 : document.querySelector('#longitud').value;
    
        const mapa = L.map('mapa').setView([lat,lng],16);

        //eliminar pines previos
        let markers = new L.FeatureGroup().addTo(mapa);
    
        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',{
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(mapa);
    
        let marker;
        
        //se agrega el pin de marcacion
        marker = new L.marker([lat,lng],{
            draggable:true,//para que se mueve el pin
            autoPan:true//para que se mueva el mapa cuando se mueve el pin
        }).addTo(mapa);

        //agregar el pin a las capas para poderlo eliminar si el usuario utiliza el buscador de lugares
        markers.addLayer(marker);

        //geocode services
        const geocodeService = L.esri.Geocoding.geocodeService();
        
        //buscador
        const buscador = document.querySelector('#formbuscador');
        buscador.addEventListener('blur',buscarDireccion);

        reubicarPin(marker);

        function reubicarPin(marker)
        {
            //detectar el movimiento del marker
            marker.on('moveend',function(e)
            {
                marker = e.target;
                const posicion = marker.getLatLng();

                // centrar mapa automaticamente
                mapa.panTo(new L.LatLng(posicion.lat,posicion.lng));

                // reverse Geocoding, cuando el usuario reubica el pin
                geocodeService.reverse().latlng(posicion,16).run(function(error, resultado)
                {
                    //console.log(error);
                    //console.log(resultado);
                    marker.bindPopup(resultado.address.LongLabel);
                    marker.openPopup();

                    //llenar campos
                    llenarInputs(resultado);

                });   
            
            });
        }

        function buscarDireccion(e)
        {
            if(e.target.value.length > 1)
            {
                //console.log("desde buscar")
                provider.search({query: e.target.value})
                .then(resultado => {
                    if(resultado)
                    {
                        //limpiar los pines previos
                        markers.clearLayers();
                        // reverse Geocoding, cuando el usuario reubica el pin
                        geocodeService.reverse().latlng(resultado[0].bounds[0],16).run(function(error, resultado){
                            //llenar los inputs
                            llenarInputs(resultado);

                            //centrar el mapa
                            mapa.setView(resultado.latlng);

                            //agregar el pin
                            marker = new L.marker(resultado.latlng,{
                                draggable:true,//para que se mueve el pin
                                autoPan:true//para que se mueva el mapa cuando se mueve el pin
                            }).addTo(mapa);

                            //asignar el contenedor de markers el nuevo pin
                            markers.addLayer(marker);

                            //mover el pin
                            reubicarPin(marker);
                        });  
            
                    }

                }).catch(error => {
                    //console.log(error);
                });

            }
        }

        function llenarInputs(resultado)
        {
            document.querySelector('#direccion').value = resultado.address.LongLabel || '';
            document.querySelector('#latitud').value = resultado.latlng.lat || '';
            document.querySelector('#longitud').value = resultado.latlng.lng || '';
        }

    }
    
});