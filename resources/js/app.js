
require('./bootstrap');
//require('./mapa');
require('./dropzone');

window.Vue = require('vue');

import router from './router';

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('pagina-inicio', require('./components/PaginaInicioComponent.vue').default);

const app = new Vue({
    el: '#app',
    router
});
