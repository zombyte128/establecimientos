//primero definimos que no se auto descubra el dropzone...esto es para que no carge antes de subir algun archivo
Dropzone.autoDiscover = false;

//antes que se arge el dropzone se tiene que cargar todo el contenido del html
document.addEventListener('DOMContentLoaded',() => {

    //una vez cargado todo el contenido del html, definimos que se ejecutara el dropzone mediavez se le de click sobre el
    if(document.querySelector('div#dropzone'))
    {
        // declaramos una variable constante que contendra las opciones y contenido del dropzone
        const dropzone = new Dropzone('div#dropzone', {
            url: '/imagenes/store',//url de la api o ruta donde se enviaran las imagenes
            dictDefaultMessage: 'Sube hasta 10 imágenes',//mensaje que mostrara el dropzone
            maxFiles:10,//maximo de imagenes que se pueden subir...aqui se definen
            required:true,//marcamos si es requerido
            acceptedFiles:".png,.jpg,.gif,.bmp,.jpeg",//los tipos de archivos que hemos admitidos
            addRemoveLinks: true,//habilitar el link de remover imagen
            dictRemoveFile: "Eliminar Imagen",//aqui modificamos el mensaje del link de modificar imagen
            dictResponseError: 'Error al subir el archivo!',//si ocurre algun error inesperado el dropzone notificara
            headers:{ //en esta seccion se tiene que declarar el csrf-token ya que sin el no podemos hacer post en laravel
                'X-CSRF-TOKEN':  document.querySelector('meta[name="csrf-token"').content//esta token lo abtenemos del meta que esta en el head en app.blade.php
            },
            init: function(){
                const galeria = document.querySelectorAll('.galeria');
                if(galeria.length > 0){
                    galeria.forEach(imagen => {
                        const imagenPublicada = {};
                        imagenPublicada.size = 1;
                        imagenPublicada.name = imagen.value;
                        imagenPublicada.nombreServidor = imagen.value;

                        this.options.addedfile.call(this, imagenPublicada);
                        this.options.thumbnail.call(this, imagenPublicada,`/storage/${imagenPublicada.name}`);

                        imagenPublicada.previewElement.classList.add('dz-success');
                        imagenPublicada.previewElement.classList.add('dz-complete');

                    });
                }

            },
            success: function(file, respuesta){//esta funcion es cuando ya se haya enviado la imagen a la base de datos
                file.nombreServidor = respuesta.archivo;//file accede a nombrar el archivo a enviar y lo igualamos a la respuesta accediendo al nombre del archivo
            },
            sending: function(file,xhr,formData){//esta funcion es para enviar los parametros a la base de datos
                formData.append("csrf-token",document.querySelector('meta[name="csrf-token"').content);//aqui enviamos el csrf-token sin esta linea de codigo no podremos subir la imagen a la base de datos
                formData.append('uuid',document.querySelector('#uuid').value);//aqui enviamos el unique id que generamos de la imagen lo guardamos en un input tipo hidden en la vista create
            },
            removedfile: function(file, respuesta)//esta funcion elimina la image de la base de datos
            {
                const params = {//delcaramos una variable constante que contendra el objeto de la imagen( su nombre y extencion)
                    imagen : file.nombreServidor,
                    uuid : document.querySelectorAll('#uuid').value
                } 

                axios.post('/imagenes/destroy',params)//enviamos la peticion por medio de post a la ruta que contiene el metodo para elminar la imagen en la api
                .then( respuesta => {
                    //eliminar del DOM para que se reinicie el dropzone
                    file.previewElement.parentNode.removeChild(file.previewElement);
                })

            }
        });

    }

});