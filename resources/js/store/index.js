import Vue from 'vue';
import Vuex from 'vuex';// vuex es solo para manejar el state de la aplicacion

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        restaurantes: [],
        bares: [],
        cafes: [],
        gimnasios: [],
        hospitales: [],
        hoteles: [],
        establecimiento:{},
        establecimientos:[],
        categorias: [],
        categoria: ''
    },
    mutations: {
        AGREGAR_RESTAURANTES(state, restaurantes){
            state.restaurantes = restaurantes;

        },
        AGREGAR_BARES(state, bares){
            state.bares = bares;

        },
        AGREGAR_CAFES(state, cafes){
            state.cafes = cafes;

        },
        AGREGAR_GIMNASIOS(state, gimnasios){
            state.gimnasios = gimnasios;

        },
        AGREGAR_HOSPITALES(state, hospitales){
            state.hospitales = hospitales;
            

        },
        AGREGAR_HOTELES(state, hoteles){
            state.hoteles = hoteles;

        },
        AGREGAR_ESTABLECIMIENTO(state, establecimiento){
            state.establecimiento = establecimiento;
        },
        AGREGAR_ESTABLECIMIENTOS(state, establecimientos){
            state.establecimientos = establecimientos;
        },
        AGREGAR_CATEGORIAS(state, categorias){
            state.categorias = categorias;
        },
        SELECCIONAR_CATEGORIA(state, categoria){
            state.categoria = categoria
        }

    },
    getters:{
        obtenerEstablecimiento: state => {
            return state.establecimiento;
        },
        obtenerImagenes: state => {
            return state.establecimiento.imagenes;
        },
        obtenerEstablecimientos: state => {
            return state.establecimientos;
        },
        obtenerCategorias: state => {
            return state.categorias;
        },
        obtenerCategoria: state => {
            return state.categoria;
        }
    }

});