@extends('layouts.app')

@section('css')
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css"
  integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
  crossorigin=""/>
<link
  rel="stylesheet"
  href="https://unpkg.com/esri-leaflet-geocoder/dist/esri-leaflet-geocoder.css"
/>
<link
  rel="stylesheet"
  href="https://unpkg.com/leaflet-geosearch@3.0.0/dist/geosearch.css"
/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.7.2/dropzone.min.css">
@endsection  


@section('content')
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css"
  integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
  crossorigin=""/>
<div class="container">
    <h1 class="text-center mt-4">Editar Establecimiento</h1>
    <div class="mt-5 row justify-content-center">
        <form action="{{ route('establecimiento.update',['establecimiento' => $establecimiento->id]) }}" method="POST" mclass="col-md-9 col-xs-12 card card-body" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <fieldset class="border p-4">
                <legend class="text-primary">Nombre,Categoría e imagen principal</legend>
                <div class="form-group">
                    <label for="nombre">Nombre establecimiento</label>
                    <input type="text" id="nombre" class="form-control @error('nombre_establecimiento') is-invalid @enderror" value="{{ $establecimiento->nombre_establecimiento }}" placeholder="ingrese nombre establecimiento" name="nombre_establecimiento">
                    @error('nombre_establecimiento')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="categorias">Categoría</label>
                    <select name="categoria_id" id="categoria" class="form-control @error('categoria_id') is-invalid @enderror">
                        <option value="" selected disabled>-- Seleccione --</option>
                        @foreach($categorias as $categoria)
                        <option value="{{ $categoria->id }}" {{ $establecimiento->categoria_id == $categoria->id ? 'selected':'' }}>{{ $categoria->nombre_categoria }}</option>
                        @endforeach
                    </select>
                    @error('categoria_id')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="nombre">Logo del establecimiento</label>
                    <input type="file" id="imagen_principal" class="form-control @error('imagen_principal') is-invalid @enderror" value="{{ old($establecimiento->imagen_principal) }}" name="imagen_principal">
                    @error('imagen_principal')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                    <img src="/storage/{{ $establecimiento->imagen_principal }}" alt="" style="width: 200px;margin-top:20px;">
                </div>
            </fieldset>
            <fieldset class="border p-4 mt-5">
                <legend class="text-primary">Ubicación</legend>
                <div class="form-group">
                    <label for="formbuscador">Coloca la ubicación de tu establecimiento</label>
                    <input type="text" id="formbuscador" class="form-control" placeholder="direccion del negocio">
                    <p class="text-secondary mt-5 mb-3 text-center">el asistente colocara una direccion estimada o mueve el pin hacia el lugar correcto</p>
                </div>
                <div class="form-group">
                    <div id="mapa" style="height: 400px">
                    </div>
                    <p class="informacion">Confirma que los siguientes campos son correctos</p>
                    <div class="form-group">
                        <label for="direccion">direccion</label>
                        <input type="text" id="direccion" class="form-control @error('direccion') is-invalid @enderror" value="{{ $establecimiento->direccion }}" placeholder="ingrese direccion" name="direccion">
                    @error('direccion')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                    </div>
                    <input type="hidden" id="latitud" name="latitud" value="{{ $establecimiento->latitud }}">
                    <input type="hidden" id="longitud" name="longitud" value="{{ $establecimiento->longitud }}">
                </div>
            </fieldset>

            <fieldset class="border p-4 mt-5">
                <legend class="text-primary">Información del establecimiento: </legend>
                <div class="form-group">
                    <label for="telefono">teléfono</label>
                    <input type="tel" class="form-control @error('numero_telefono') is-invalid @enderror" id="telefono" placeholder="Ingrese el teléfono del establecimiento" name="numero_telefono" value="{{ $establecimiento->numero_telefono }}">
                    @error('numero_telefono')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="descripcion">Descripción</label>
                    <textarea class="form-control @error('descripcion') is-invalid @enderror" name="descripcion" id="descripcion">{{ $establecimiento->descripcion }}</textarea>
                    @error('descripcion')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="hora_apertura">Hora apertura:</label>
                    <input type="time" class="form-control @error('hora_apertura') is-invalid @enderror" id="apertura" name="hora_apertura" value="{{ $establecimiento->hora_apertura }}">
                    @error('hora_apertura')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="hora_cierre">Hora apertura:</label>
                    <input type="time" class="form-control @error('hora_cierre') is-invalid @enderror" id="cierre" name="hora_cierre" value="{{ $establecimiento->hora_cierre }}">
                    @error('hora_cierre')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
            </fieldset>

            <fieldset class="border p-4 mt-5">
                <legend class="text-primary">Imagenes del establecimiento: </legend>
                <div class="form-group">
                    <label for="imagenes">Imagenes</label>
                    <div class="dropzone form-control" id="dropzone"></div>
                </div>

                @if(count($imagenes) > 0)
                   @foreach($imagenes as $imagen)
                   <input type="hidden" class="galeria" value="{{ $imagen->ruta_imagen }}">
                   @endforeach
                @endif
            </fieldset>    
            
            <input type="hidden" id="uuid" name="uuid" value="{{ $establecimiento->uuid }}">
            <input type="submit" class="btn btn-primary mt-3 d-block" value="Guardar Cambios establecimiento">

        </form>
    </div>
   
</div>
@endsection

@section('js')
<script type="application/javascript" src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"
  integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew=="
  crossorigin="" defer></script>
<script type="application/javascript" src="https://unpkg.com/esri-leaflet" defer></script>
<script type="application/javascript" src="https://unpkg.com/esri-leaflet-geocoder" defer></script>
<script type="application/javascript" src="https://unpkg.com/leaflet-geosearch@3.0.0/dist/geosearch.umd.js" defer></script>
<script type="application/javascript" src="{{ asset('js/mapa.js') }}"></script>

@endsection