<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/establecimientos','ApiController@index')->name('establecimientos.index');
route::get('/establecimientos/{establecimiento}', 'ApiController@show')->name('establecimientos.show');
Route::get('/categorias','ApiController@categorias')->name('categorias');
Route::get('/categorias/{categoria}','ApiController@categoria')->name('categoria');
Route::get('/{categoria}','ApiController@establecimientocategoria')->name('establecimientocategoria');
