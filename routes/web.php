<?php

use Illuminate\Support\Facades\Route;

Route::get('/','InicioController')->name('inicio');

/** todas las rutas que contengan auth sera necesario que el usuario haya verificad el email */
Auth::routes(['verify' => true]);

/** solo los usuarios que hayan verificado su email podran acceder a estas rutas */
Route::group(['middleware' => ['auth','verified']],function(){
    Route::get('/establecimientos/create','EstablecimientoController@create')->name('establecimiento.create')->middleware('revisar');
    Route::get('/establecimientos/edit','EstablecimientoController@edit')->name('establecimiento.edit');
    Route::post('/establecimientos/store','EstablecimientoController@store')->name('establecimiento.store');
    Route::put('/establecimientos/{establecimiento}','EstablecimientoController@update')->name('establecimiento.update');

    Route::post('imagenes/store','ImagenController@store')->name('imagenes.store');
    Route::post('/imagenes/destroy','ImagenController@destroy')->name('imagenes.destroy');

});


